#include "stm32f4xx.h"
#include "led.h"

void led_init(void) {
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    __HAL_RCC_GPIOG_CLK_ENABLE();
    GPIO_InitStruct.Pin = GPIO_PIN_13 | GPIO_PIN_14;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

    HAL_GPIO_WritePin(GPIOG, GPIO_PIN_13, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOG, GPIO_PIN_14, GPIO_PIN_RESET);
}

/**
 * led control
 * @param hd: led handle, see @led_t
 * @param cmd: the command, 1 --- lights up; 0 --- lights off
 */
int32_t led_ctrl(uint32_t hd, uint32_t cmd) {
    switch (hd) {
    case LED1:
        HAL_GPIO_WritePin(GPIOG, GPIO_PIN_13, cmd > 0 ? GPIO_PIN_SET : GPIO_PIN_RESET);
        break;

    case LED2:
        HAL_GPIO_WritePin(GPIOG, GPIO_PIN_14, cmd > 0 ? GPIO_PIN_SET : GPIO_PIN_RESET);
        break;

    default:
        return 1;
    }
    return 0;
}
