#ifndef _LED_H_
#define _LED_H_

#include <stdint.h>

enum {
    LEDNONE,
    LED1,
    LED2,
} led_t;

void led_init(void);
int32_t led_ctrl(uint32_t hd, uint32_t cmd);

#endif

