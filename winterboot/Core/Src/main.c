/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "gpio.h"
#include "fmc.h"
#include "fatfs.h"
#include "sdio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <string.h>
#include "sdram.h"
#include "led.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
//=================================================================================
#define SDRAM_ADDR      (0x90000000)
#define SDRAM_SIZE      (8 * 1024 * 1024)	/* bytes */
#define ERR(x)          if ((x)) { return; }

/*
 * for linux
 */
#define CONFIG_DTBADDR        (SDRAM_ADDR)
#define CONFIG_KERADDR        (SDRAM_ADDR + 0x8000)
#define CONFIG_DTBFN          "devicetree.bin"
#define CONFIG_KERFN          "kernel.bin"

/*
 * for normal firmware
 */
#define CONFIG_APPADDR        (SDRAM_ADDR)
#define CONFIG_APPFN          "app.bin"

int32_t is_sdram_work_well(uint32_t addr, uint32_t size) {
    uint32_t i, j;
    uint8_t * const p = (uint8_t *)addr;
    uint32_t bytes = size;
    uint32_t halfwords = size / 2;
    uint32_t words = size / 4;
    uint32_t doubles = size / 8;

    // testing access whole SDRAM in unit: byte
    for (i = 0; i < bytes; i++) {
    	p[i] = i;
    }
    for (j = 0, i = 0; i < bytes; i++) {
        if (p[i] != j) {
        	goto error;
        }
        if (++j >= 256) {
        	j = 0;
        }
    }

    // testing access whole SDRAM in unit: halfword
    for (i = 0; i < halfwords; i++) {
    	*(uint16_t *)&p[i * 2] = i;
    }
    for (j = 0, i = 0; i < halfwords; i++) {
        if (*(uint16_t *)&p[i * 2] != j) {
        	goto error;
        }
        if (++j >= 65536) {
        	j = 0;
        }
    }

    // testing access whole SDRAM in unit: word
    for (i = 0; i < words; i++) {
    	*(uint32_t *)&p[i * 4] = i;
    }
    for (i = 0; i < words; i++) {
        if (*(uint32_t *)&p[i * 4] != i) {
        	goto error;
        }
    }

    // testing access whole SDRAM in unit: double
    for (i = 0; i < doubles; i++) {
    	*(uint64_t *)&p[i * 8] = i;
    }
    for (i = 0; i < doubles; i++) {
        if (*(uint64_t *)&p[i * 8] != i) {
        	goto error;
        }
    }

    // set all SDRAM memory to empty
    for (i = 0; i < bytes; i++) {
    	p[i] = 0xFF;
    }
    for (i = 0; i < bytes; i++) {
        if (p[i] != 0xFF) {
        	goto error;
        }
    }

    return 1;

error:
    return 0;
}

static int32_t check_sd(void) {
#define TEST_FILE       "0:check_sd.txt"
#define TEST_STR        "0123456789ABCDEF"

    static FATFS FatFs;
    static FIL fil;
    const char *p = TEST_STR;
    char buf[sizeof(TEST_STR)];
    UINT bw, br;

    if (f_mount(&FatFs, "0:", 1)) {
        return 1;
    }

    if (f_open(&fil, TEST_FILE, FA_WRITE | FA_CREATE_ALWAYS)) {
        return 1;
    }
    if (f_write(&fil, p, strlen(p), &bw) || bw != strlen(p)) {
        goto error;
    }
    f_close(&fil);

    if (f_open(&fil, TEST_FILE, FA_READ)) {
        goto delete;
    }
    if (f_read(&fil, buf, sizeof(buf), &br) || br != strlen(p)) {
        goto error;
    }
    f_close(&fil);
    if (f_stat(TEST_FILE, NULL) == FR_OK) {
        f_unlink(TEST_FILE);
    }

    buf[br >= sizeof(buf) ? sizeof(buf) - 1 : br] = 0;
    if (strcmp(p, buf)) {
        return 3;
    }
    return 0;

error:
    f_close(&fil);
delete:
    if (f_stat(TEST_FILE, NULL) == FR_OK) {
        f_unlink(TEST_FILE);
    }
    return 2;
}

static void gen_readme(void) {
    static FIL fil;

    if (f_open(&fil, "HOWTOUSE.txt", FA_CREATE_ALWAYS | FA_WRITE)) {
        return;
    }

    f_printf(&fil, "This sdcard can boot Linux or APP(normal Cortex-CM* firmware) if you put files like following:\n");
    f_printf(&fil, "\n");
    f_printf(&fil, "If booting Linux\n");
    f_printf(&fil, "=================\n");
    f_printf(&fil, "    %s(0x%08x)\n", CONFIG_DTBFN, CONFIG_DTBADDR);
    f_printf(&fil, "    %s(0x%08x)\n", CONFIG_KERFN, CONFIG_KERADDR);
    f_printf(&fil, "\n");
    f_printf(&fil, "If booting APP\n");
    f_printf(&fil, "===============\n");
    f_printf(&fil, "    %s(0x%08x)\n", CONFIG_APPFN, CONFIG_APPADDR);
    f_printf(&fil, "\n");
    f_printf(&fil, "Note\n");
    f_printf(&fil, "=====\n");
    f_printf(&fil, "    if Linux and APP is both existing, the APP is prior to boot\n");

    f_close(&fil);
}

static void move_and_jump(void) {
    static FIL fil;
    FRESULT res;
    unsigned int br;
    unsigned char *dtb;
    unsigned char *ker;
    enum {FIR_NONE, FIR_APP, FIR_LINUX} firmware;

    // check sdcard
    if (check_sd()) {
        return;
    }
    gen_readme();

    // check SDRAM
    LL_SYSCFG_EnableFMCMemorySwapping();
    HAL_Delay(1000);			// the swaping of SDRAM and NOR/RAM takes some time
    if (!is_sdram_work_well(SDRAM_ADDR, SDRAM_SIZE)) {
    	return;
    }

    // load firmware
    if (f_stat(CONFIG_APPFN, NULL) == FR_OK) {
        firmware = FIR_APP;
        ker = (unsigned char *)CONFIG_APPADDR;

        res = f_open(&fil, CONFIG_APPFN, FA_READ); ERR(res);
        res = f_read(&fil, ker, f_size(&fil), &br); ERR(res);
        res = f_close(&fil); ERR(res);
    } else if (f_stat(CONFIG_DTBFN, NULL) == FR_OK && f_stat(CONFIG_KERFN, NULL) == FR_OK) {
        firmware = FIR_LINUX;
        dtb = (unsigned char *)CONFIG_DTBADDR;
        ker = (unsigned char *)CONFIG_KERADDR;

        res = f_open(&fil, CONFIG_DTBFN, FA_READ); ERR(res);
        res = f_read(&fil, dtb, f_size(&fil), &br); ERR(res);
        res = f_close(&fil); ERR(res);

        res = f_open(&fil, CONFIG_KERFN, FA_READ); ERR(res);
        res = f_read(&fil, ker, f_size(&fil), &br); ERR(res);
        res = f_close(&fil); ERR(res);
    } else {
        firmware = FIR_NONE;
        return;
    }

    // disable all irq
    *(volatile unsigned int*)0xE000E010 &= ~1;      // disable systick irq

    /*
     * you may need to disable I and D cache before jumping
     *
     * SCB_CleanDCache();
     * SCB_DisableDCache();
     * SCB_InvalidateICache();
     * SCB_DisableICache();
     */

    // jump to kernel
    led_ctrl(LED1, 0);
    led_ctrl(LED2, 0);
    if (firmware == FIR_LINUX) {
        typedef void (*func_t)(uint32_t reserved, uint32_t mach, uint32_t dt);
        static func_t kernel;

        kernel = (func_t)(CONFIG_KERADDR | 1);
        kernel(0, ~0UL, CONFIG_DTBADDR);
    } else if (firmware == FIR_APP) {
        typedef void (*func_t)(void);
        static func_t kernel;

        kernel = (func_t)(*(unsigned int *)(CONFIG_APPADDR + 4));
        __set_MSP(*(unsigned int *)CONFIG_APPADDR);
        kernel();
    }
}
//---------------------------------------------------------------------------------
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */


  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_FMC_Init();
  BSP_SDRAM_Initialization_sequence(REFRESH_COUNT);
  MX_SDIO_SD_Init();
  MX_FATFS_Init();
  /* USER CODE BEGIN 2 */
  led_init();
  led_ctrl(LED1, 1);
  led_ctrl(LED2, 1);
  move_and_jump();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 360;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
