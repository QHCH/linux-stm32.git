#!/bin/bash

BIN=`find . -name *.bin`

openocd -f board/stm32f429discovery.cfg -c "program $BIN verify reset exit 0x08000000"
