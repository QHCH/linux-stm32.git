# What's this

This is a Linux development SDK running on STM32F429I-Discovery.
The winterboot in the SDK is a bootloader loads the linux kernel and device
tree from tfcard into SDRAM then runing it. However, the board,
STM32F429I-Discovery, has no tfcard adapter and it's in-board STLink doesn't
support USB2UART, so you need to install a tfcard adapter and a USB2UART
dongle yourself.

## 1. How to build
The toolchain, "gcc-arm-none-eabi-5_4-2016q3-20160926-linux.tar.bz2" in the
SDK must be installed before you start.

1. $ cd winterboot
2. $ make
3. $ ./burnflash.sh
4. $ bzip2 -d Stm32_mini_rootfs.cpio.bz2
5. $ mkdir rootfs && cd rootfs
6. $ sudo cpio -idmv < ../Stm32_mini_rootfs.cpio
7. $ touch init && chmod +x init && vim init  # write the following text in
```
#!/bin/sh
# devtmpfs does not get automounted for initramfs
/bin/mount -t devtmpfs devtmpfs /dev
exec 0</dev/console
exec 1>/dev/console
exec 2>/dev/console
exec /sbin/init $
```

8. $ cd ..
9. $ tar xf linux-4.13.12.tar.xz
10. $ cd linux-4.13.12
11. $ make ARCH=arm CROSS_COMPILE=arm-none-eabi- stm32_defconfig
12. $ make ARCH=arm menuconfig  # the rootfs is attached to kernel
```
General setup  --->
	[*] Initial RAM filesystem and RAM disk (initramfs/initrd) support
		(<input your rootfs absolut path here>) Initramfs source file(s)

Boot options  --->
    [*] Kernel Execute-In-Place from ROM
    (0x90008000) XIP Kernel Physical Location

System Type  --->
    [*] Set flash/sdram size and base addr
    (0x90400000) (S)DRAM Base Address
    (0x00400000) (S)DRAM SIZE
    (0x90000000) FLASH Base Address
    (0x00400000) FLASH Size

# Note: using shift+backspace to delete
```
13. $ make ARCH=arm CROSS_COMPILE=arm-none-eabi-    # you will get xipImage and stm32f429-disco.dtb
14. copy xipImage and stm32f429-disco.dtb from linux into the tfcard's root path as kernel.bin and devicetree.bin
15. inset the tfcard into STM32F429I-Discovery board, connect the USART1 with PC, then reset the board
16. enjoy it

The image layout would be this:

| file   | address |
|--------|--------|
| bootloader  |  0x8000000   |
| devicetree  |  0x8004000   |
| kernel      |  0x8008000   |

## 2. How to debug the Linux
1. $ cd winterboot
2. $ openocd
3. $ # open another SSH window and goto winterboot direction
4. $ ./gdb.sh
5. (gdb) b main.c:291   # stop before jumping to linux
6. (gdb) c
7. (gdb) file ~/linux-stm32/linux-4.13.12/vmlinux  # you will need to enter 'y' to confirm your operation here
8. (gdb) b _text        # stop on the entry of linux
9. (gdb) c
10. enjoy it
